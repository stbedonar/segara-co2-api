# Segara CO2 REST API

REST API of the Segara Emission Control System (SegaraCO2).

## Development

Ensure you have a [PostgreSQL server running as a docker-container](https://dev.to/shree_j/how-to-install-and-run-psql-using-docker-41j2) on the same machine, with the following details:

- Database: `segara`.
- User: `segara_user`.
- Password: `123456`.
- Port: `5432`.

Run:

```
uvicorn app.main:app --reload
```

## Populate the database

If it is your first time running the app, create default and example data:

```
python -m app.initial_data
```
