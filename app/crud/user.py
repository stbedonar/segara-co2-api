from sqlalchemy.orm import Session

from app import models

from app.schemas.user import User


def create_user(db:Session, user: User) -> models.User:
    db_user = models.User(
        email=user.email,
        name=user.name,
        last_name=user.last_name,
        hashed_password=user.hashed_password,
        is_active=user.is_active,
        role_id=user.role_id,
        organisation_name=user.organisation_name)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def get_user_by_id(db:Session, id: int) -> models.User:
    return db.query(models.User).filter(models.User.id == id).first()


def get_user_by_email(db:Session, email: str) -> models.User:
    return db.query(models.User).filter(models.User.email == email).first()


def get_user_by_name_and_last_name(db:Session, name: str, last_name: str) -> models.User:
    return db.query(models.User)\
        .filter(models.User.name == name, models.User.last_name == last_name)\
        .first()


def get_active_or_unactive_users(db:Session, is_active: bool) -> models.User:
    return db.query(models.User)\
        .filter(models.User.is_active == is_active)\
        .order_by(models.User.last_name.asc())


def get_users_by_organisation_id(db:Session, id: int) -> models.User:
    return db.query(models.User)\
        .filter(models.User.organisation_id == id)\
        .order_by(models.User.last_name.asc())


def get_users_by_role_id(db:Session, id: int) -> models.User:
    return db.query(models.User)\
        .filter(models.User.role_id == id)\
        .order_by(models.User.last_name.asc())


def get_users(db:Session, skip: int = 0, limit: int = 100) -> models.User:
    return db.query(models.User).offset(skip).limit(limit).all()


def update_user(db:Session, id: int, user: User) -> models.User:
    db_user = get_user_by_id(db, id)
    db_user = models.User(
        email=user.email,
        name=user.name,
        last_name=user.last_name,
        hashed_password=user.hashed_password,
        is_active=user.is_active,
        role_id=user.role_id,
        organisation_id=user.organisation_id)
    db.commit()
    db.refresh(db_user)
    return db_user


def delete_user(db:Session, id: int) -> models.User:
    db_user = get_user_by_id(db, id)
    db.delete(db_user)
    db.commit()
    return db_user