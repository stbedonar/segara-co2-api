from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app import models

from app.schemas.fuel import Fuel


def create_fuel(db:Session, fuel: Fuel) -> models.Fuel:
    db_fuel = models.Fuel(
        name=fuel.name,
        year=fuel.year,
        emission_factor=fuel.emission_factor)
    db.add(db_fuel)
    db.commit()
    db.refresh(db_fuel)
    return db_fuel


def get_fuel_by_id(db:Session, id: int) -> models.Fuel:
    return db.query(models.Fuel).filter(models.Fuel.id == id).first()


def get_fuel_by_name(db:Session, name: str) -> models.Fuel:
    return db.query(models.Fuel)\
        .filter(models.Fuel.name == name)\
        .order_by(models.Fuel.year.asc())\
        .all()


def get_fuels_by_year(db:Session, year: int) -> models.Fuel:
    return db.query(models.Fuel)\
        .filter(models.Fuel.year == year)\
        .order_by(models.Fuel.name.asc())\
        .all()


def get_fuel_by_name_and_year(db: Session, name: str, year: int) -> models.Fuel:
    return db.query(models.Fuel).filter(models.Fuel.name == name, models.Fuel.year == year).first()


def get_fuels_by_emission_factor(db:Session, emission_factor: float, operator: str) -> models.Fuel:
    if operator == "gt":
        return db.query(models.Fuel)\
        .filter(models.Fuel.emission_factor > emission_factor)\
        .order_by(models.Fuel.emission_factor.asc())\
        .all()
    if operator == "lt":
        return db.query(models.Fuel)\
        .filter(models.Fuel.emission_factor < emission_factor)\
        .order_by(models.Fuel.emission_factor.asc())\
        .all()
    if operator == "et":
        return db.query(models.Fuel)\
        .filter(models.Fuel.emission_factor == emission_factor)\
        .order_by(models.Fuel.emission_factor.asc())\
        .all()


def get_fuels(db:Session, skip: int = 0, limit: int = 100) -> models.Fuel:
    return db.query(models.Fuel).offset(skip).limit(limit).all()


def update_fuel(db:Session, id: int, fuel: Fuel) -> models.Fuel:
    db_fuel = get_fuel_by_id(db, id)
    fuel_data = jsonable_encoder(db_fuel)
    fuel = fuel.dict(skip_defaults=True)
    for field in fuel_data:
        if field in fuel:
            setattr(db_fuel, field, fuel[field])
    db.commit()
    db.refresh(db_fuel)
    return db_fuel


def delete_fuel(db:Session, id: int) -> models.Fuel:
    db_fuel = get_fuel_by_id(db, id)
    db.delete(db_fuel)
    db.commit()
    return db_fuel