from sqlalchemy.orm import Session

from app import models

from app.schemas.revenue import Revenue, RevenueCreate


def create_revenue(db:Session, revenue: RevenueCreate) -> models.Revenue:
    db_revenue = models.Revenue(year=revenue.year, total=revenue.total)
    db.add(db_revenue)
    db.commit()
    db.refresh(db_revenue)
    return db_revenue


def get_revenue_by_id(db:Session, id: int) -> models.Revenue:
    return db.query(models.Revenue).filter(models.Revenue.id == id).first()


def get_revenue_by_year(db:Session, year: int) -> models.Revenue:
    return db.query(models.Revenue).filter(models.Revenue.year == year).first()


def get_revenues(db:Session, skip: int = 0, limit: int = 100) -> models.Revenue:
    return db.query(models.Revenue).offset(skip).limit(limit).all()


def update_revenue(
    db:Session, id: int, revenue: Revenue
    ) -> models.Revenue:
    db_revenue = get_revenue_by_id(db, id)
    revenue_data = jsonable_encoder(db_revenue)
    revenue = revenue.dict(skip_defaults=True)
    for field in revenue_data:
        if field in revenue:
            setattr(db_revenue, field, revenue[field])
    db.commit()
    db.refresh(db_revenue)
    return db_revenue


def delete_revenue(db:Session, id: int) -> models.Revenue:
    db_revenue = get_revenue_by_id(db, id)
    db.delete(db_revenue)
    db.commit()
    return db_revenue