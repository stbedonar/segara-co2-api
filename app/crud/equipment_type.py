from sqlalchemy.orm import Session

from app import models

from app.schemas.equipment_type import EquipmentType, EquipmentTypeCreate


def create_equipment_type(db:Session, equipment_type: EquipmentTypeCreate) -> models.EquipmentType:
    db_equipment_type = models.EquipmentType(name=equipment_type.name)
    db.add(db_equipment_type)
    db.commit()
    db.refresh(db_equipment_type)
    return db_equipment_type


def get_equipment_type_by_id(db:Session, id: int) -> models.EquipmentType:
    return db.query(models.EquipmentType).filter(models.EquipmentType.id == id).first()


def get_equipment_type_by_name(db:Session, name: str) -> models.EquipmentType:
    return db.query(models.EquipmentType).filter(models.EquipmentType.name == name).first()


def get_equipment_types(db:Session, skip: int = 0, limit: int = 100) -> models.EquipmentType:
    return db.query(models.EquipmentType).offset(skip).limit(limit).all()


def update_equipment_type(
    db:Session, id: int, equipment_type: EquipmentType
    ) -> models.EquipmentType:
    db_equipment_type = get_equipment_type_by_id(db, id)
    db_equipment_type.name = equipment_type.name
    db.commit()
    db.refresh(db_equipment_type)
    return db_equipment_type


def delete_equipment_type(db:Session, id: int) -> models.EquipmentType:
    db_equipment_type = get_equipment_type_by_id(db, id)
    db.delete(db_equipment_type)
    db.commit()
    return db_equipment_type