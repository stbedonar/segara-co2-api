from datetime import date

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy.sql import func

from app import models, calculator

from app.schemas.stationary_combustion_invoice import (
    StationaryCombustionInvoice, StationaryCombustionInvoiceCreate
    )

from . import fuel, facility


def create_invoice(
    db:Session, invoice: StationaryCombustionInvoiceCreate
    ) -> models.StationaryCombustionInvoice:

    # End user doesn't know the fuel id, so we get it from the database
    # using the provided invoice date and fuel name
    year = invoice.invoice_date.year
    fuel_id = fuel.get_fuel_by_name_and_year(db, invoice.fuel_name, year).id

    partial = calculator.partial_combustion_invoice(db, fuel_id, invoice.units)

    db_invoice = models.StationaryCombustionInvoice(
        invoice_number=invoice.invoice_number,
        invoice_date=invoice.invoice_date,
        units=invoice.units,
        total=invoice.total,
        fuel_id=fuel_id,
        facility_name=invoice.facility_name,
        partial_co2=partial)
    db.add(db_invoice)
    db.commit()
    db.refresh(db_invoice)
    return db_invoice


def get_invoice_by_id(db:Session, id: int) -> models.StationaryCombustionInvoice:
    return db.query(models.StationaryCombustionInvoice)\
        .filter(models.StationaryCombustionInvoice.id == id)\
        .first()


def get_invoice_by_invoice_number(db:Session, number: str) -> models.StationaryCombustionInvoice:
    return db.query(models.StationaryCombustionInvoice)\
        .filter(models.StationaryCombustionInvoice.invoice_number == number)\
        .first()


def get_invoices_between_dates(
    db:Session, date1: date, date2: date
    ) -> models.StationaryCombustionInvoice:
    return db.query(models.StationaryCombustionInvoice)\
        .filter(models.StationaryCombustionInvoice.invoice_date.between(date1, date2))\
        .order_by(models.StationaryCombustionInvoice.invoice_date.asc())\
        .all()


def get_invoices_by_facility_id(db:Session, id: str) -> models.StationaryCombustionInvoice:
    return db.query(models.StationaryCombustionInvoice)\
        .filter(models.StationaryCombustionInvoice.facility_id == id)\
        .order_by(models.StationaryCombustionInvoice.invoice_date.asc())\
        .all()


def get_invoices_by_fuel_id(db:Session, id: int) -> models.StationaryCombustionInvoice:
    return db.query(models.StationaryCombustionInvoice)\
        .filter(models.StationaryCombustionInvoice.fuel_id == id)\
        .order_by(models.StationaryCombustionInvoice.invoice_date.asc())\
        .all()
    

def get_invoices_by_partial(
    db:Session, partial: float, operator: str
    ) -> models.StationaryCombustionInvoice:
    if operator == "gt":
        return db.query(models.StationaryCombustionInvoice)\
        .filter(models.StationaryCombustionInvoice.partial_co2 > partial)\
        .order_by(models.StationaryCombustionInvoice.partial_co2.asc())\
        .all()
    if operator == "lt":
        return db.query(models.StationaryCombustionInvoice)\
        .filter(models.StationaryCombustionInvoice.partial_co2 < partial)\
        .order_by(models.StationaryCombustionInvoice.partial_co2.asc())\
        .all()
    if operator == "et":
        return db.query(models.StationaryCombustionInvoice)\
        .filter(models.StationaryCombustionInvoice.partial_co2 == partial)\
        .order_by(models.StationaryCombustionInvoice.partial_co2.asc())\
        .all()


def get_invoices(
    db:Session, skip: int = 0, limit: int = 100
    ) -> models.StationaryCombustionInvoice:
    return db.query(models.StationaryCombustionInvoice).offset(skip).limit(limit).all()


def update_invoice(
    db:Session, id: int, invoice: StationaryCombustionInvoice
    ) -> models.StationaryCombustionInvoice:
    db_invoice = get_invoice_by_id(db, id)
    invoice_data = jsonable_encoder(db_invoice)
    invoice = invoice.dict(skip_defaults=True)
    for field in invoice_data:
        if field in invoice:
            setattr(db_invoice, field, invoice[field])
    partial = calculator.partial_combustion_invoice(db, db_invoice.fuel_id, db_invoice.units)
    db_invoice.partial_co2 = partial
    db.commit()
    db.refresh(db_invoice)
    return db_invoice


def delete_invoice(db:Session, id: int) -> models.StationaryCombustionInvoice:
    db_invoice = get_invoice_by_id(db, id)
    db.delete(db_invoice)
    db.commit()
    return db_invoice


def sum_partials(db:Session, date1, date2):
    return db.query(func.sum(models.StationaryCombustionInvoice.partial_co2))\
        .filter(models.StationaryCombustionInvoice.invoice_date.between(date1, date2))\
        .scalar()