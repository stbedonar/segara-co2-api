from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy import asc

from app import models

from app.schemas.equipment import Equipment

from . import equipment_type


def create_equipment(db:Session, equipment: Equipment) -> models.Equipment:

    # Checks whether or not the equipment type name already exists
    # and creates it not found in the database
    if not equipment_type.get_equipment_type_by_name(db, equipment.equipment_type_name):
        db_equipment_type = models.EquipmentType(name=equipment.equipment_type_name)
        equipment_type.create_equipment_type(db, db_equipment_type)

    db_equipment = models.Equipment(
        nickname=equipment.nickname,
        facility_name=equipment.facility_name,
        equipment_type_name=equipment.equipment_type_name,
        outliers=equipment.outliers)
    db.add(db_equipment)
    db.commit()
    db.refresh(db_equipment)
    return db_equipment


def get_equipment_by_id(db:Session, id: int) -> models.Equipment:
    return db.query(models.Equipment).filter(models.Equipment.id == id).first()


def get_equipment_by_nickname(db:Session, nickname: str) -> models.Equipment:
    return db.query(models.Equipment).filter(models.Equipment.nickname == nickname).first()


def get_equipments_by_facility_id(db:Session, id: str) -> models.Equipment:
    return db.query(models.Equipment)\
        .filter(models.Equipment.facility_id == id)\
        .order_by(models.Equipment.nickname.asc())\
        .all()


def get_equipments_by_equipment_type_id(db:Session, id: str) -> models.Equipment:
    return db.query(models.Equipment)\
        .filter(models.Equipment.equipment_type_id == id)\
        .order_by(models.Equipment.nickname.asc())\
        .all()


def get_equipments(db:Session, skip: int = 0, limit: int = 100) -> models.Equipment:
    return db.query(models.Equipment).offset(skip).limit(limit).all()


def update_equipment(db:Session, id: int, equipment: Equipment) -> models.Equipment:
    db_equipment = get_equipment_by_id(db, id)
    equipment_data = jsonable_encoder(db_equipment)
    equipment = equipment.dict(skip_defaults=True)
    for field in equipment_data:
        if field in equipment:
            setattr(db_equipment, field, equipment[field])
    db.commit()
    db.refresh(db_equipment)
    return db_equipment


def delete_equipment(db:Session, id: int) -> models.Equipment:
    db_equipment = get_equipment_by_id(db, id)
    db.delete(db_equipment)
    db.commit()
    return db_equipment