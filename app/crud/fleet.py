from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app import models

from app.schemas.fleet import Fleet, FleetCreate


def create_fleet(db:Session, fleet: FleetCreate) -> models.Fleet:
    db_fleet = models.Fleet(
        name=fleet.name,
        facility_name=fleet.facility_name)
    db.add(db_fleet)
    db.commit()
    db.refresh(db_fleet)
    return db_fleet


def get_fleet_by_id(db:Session, id: int) -> models.Fleet:
    return db.query(models.Fleet).filter(models.Fleet.id == id).first()


def get_fleet_by_name(db:Session, name: str) -> models.Fleet:
    return db.query(models.Fleet).filter(models.Fleet.name == name).first()


def get_fleets_by_organisation(db:Session, id: int) -> models.Fleet:
    return db.query(models.Fleet)\
        .filter(models.Fleet.organisation_id == id)\
        .order_by(models.Fleet.name.asc())\
        .all()


def get_fleets_by_facility(db:Session, id: int) -> models.Fleet:
    return db.query(models.Fleet)\
        .filter(models.Fleet.facility_id == id)\
        .order_by(models.Fleet.name.asc())


def get_fleets(db:Session, skip: int = 0, limit: int = 100) -> models.Fleet:
    return db.query(models.Fleet).offset(skip).limit(limit).all()


def update_fleet(db:Session, id: int, fleet: Fleet) -> models.Fleet:
    db_fleet = get_fleet_by_id(db, id)
    fleet_data = jsonable_encoder(db_fleet)
    fleet = fleet.dict(skip_defaults=True)
    for field in fleet_data:
        if field in fleet:
            setattr(db_fleet, field, fleet[field])
    db.commit()
    db.refresh(db_fleet)
    return db_fleet


def delete_fleet(db:Session, id: int) -> models.Fleet:
    db_fleet = get_fleet_by_id(db, id)
    db.delete(db_fleet)
    db.commit()
    return db_fleet