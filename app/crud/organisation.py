from sqlalchemy.orm import Session

from app import models

from app.schemas.organisation import Organisation, OrganisationCreate


def create_organisation(
    db:Session, organisation: OrganisationCreate
    ) -> models.Organisation:
    db_organisation = models.Organisation(
        name=organisation.name,
        logo=organisation.logo,
        sector_name=organisation.sector_name,
        base_year=organisation.base_year
        )
    db.add(db_organisation)
    db.commit()
    db.refresh(db_organisation)
    return db_organisation


def get_organisation_by_id(db:Session, id: int) -> models.Organisation:
    return db.query(models.Organisation).filter(models.Organisation.id == id).first()


def get_organisation_by_name(db:Session, organisation_name: str) -> models.Organisation:
    return db.query(models.Organisation)\
        .filter(models.Organisation.name == organisation_name)\
        .first()


def get_organisations(db:Session, skip: int = 0, limit: int = 100) -> models.Organisation:
    return db.query(models.Organisation).offset(skip).limit(limit).all()


def update_organisation(
    db:Session, id: int, organisation: Organisation
    ) -> models.Organisation:
    db_organisation = get_organisation_by_id(db, id)
    organisation_data = jsonable_encoder(db_organisation)
    organisation = organisation.dict(skip_defaults=True)
    for field in organisation_data:
        if field in organisation:
            setattr(db_organisation, field, organisation[field])
    db.commit()
    db.refresh(db_organisation)
    return db_organisation


def delete_organisation(db:Session, id: int) -> models.Organisation:
    db_organisation = get_organisation_by_id(db, id)
    db.delete(db_organisation)
    db.commit()
    return db_organisation