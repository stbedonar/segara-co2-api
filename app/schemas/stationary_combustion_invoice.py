from datetime import date
from typing import Optional
from pydantic import PositiveFloat, validator

from .invoice import Invoice
from .fuel import Fuel


class StationaryCombustionInvoice(Invoice):
    fuel_name: Optional[str] = None
    facility_name: Optional[str] = None


class StationaryCombustionInvoiceCreate(StationaryCombustionInvoice):
    invoice_number: str
    invoice_date: date  
    units: PositiveFloat
    fuel_name: str
    facility_name: str


class StationaryCombustionInvoiceRead(Invoice):
    id: int
    fuel: Fuel
    facility_name: str
    partial_co2: float


class StationaryCombustionInvoiceDelete(Invoice):
    id: int
    fuel_id: int
    facility_name: str
    partial_co2: float