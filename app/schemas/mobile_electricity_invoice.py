from datetime import date
from typing import Optional
from pydantic import PositiveFloat, validator

from .invoice import Invoice
from .supplier import Supplier


class MobileElectricityInvoice(Invoice):
    supplier_name: Optional[str] = None
    vehicle_license_plate: Optional[str] = None


class MobileElectricityInvoiceCreate(MobileElectricityInvoice):
    invoice_number: str
    invoice_date: date  
    units: PositiveFloat
    supplier_name: str
    vehicle_license_plate: str


class MobileElectricityInvoiceRead(Invoice):
    id: int
    supplier: Supplier
    vehicle_license_plate: str
    partial_co2: float


class MobileElectricityInvoiceDelete(Invoice):
    id: int
    supplier_id: int
    vehicle_license_plate: str
    partial_co2: float