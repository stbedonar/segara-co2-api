from typing import List, Optional
from pydantic import BaseModel, confloat, constr, validator
from . import (
    stationary_electricity_invoice, stationary_combustion_invoice,
    equipment
    )


class Facility(BaseModel):
    name: Optional[constr(min_length=1, max_length=120)] = None
    organisation_name: Optional[str] = None
    latitude: Optional[confloat(gt=-90, lt=90)] = None
    longitude: Optional[confloat(gt=-180, lt=180)] = None
    address: Optional[constr(min_length=1, max_length=120)] = None
    zip_code: Optional[constr(min_length=1, max_length=20)] = None
    city: Optional[constr(min_length=1, max_length=120)] = None
    country: Optional[constr(min_length=4, max_length=60)] = None
    outliers_combustion: Optional[List[float]] = []
    outliers_electricity: Optional[List[float]] = []

    class Config:
        orm_mode = True


class FacilityCreate(Facility):
    name: constr(min_length=1, max_length=120)
    organisation_name: str
    address: constr(min_length=1, max_length=120)
    zip_code: constr(min_length=1, max_length=20)
    city: constr(min_length=1, max_length=120)
    country: constr(min_length=4, max_length=60)


class FacilityRead(Facility):
    id: int
    stationary_combustion_invoices: List[
        stationary_combustion_invoice.StationaryCombustionInvoice
        ] = []
    stationary_electricity_invoices: List[
        stationary_electricity_invoice.StationaryElectricityInvoice
        ] = []
    equipments: List[equipment.Equipment] = []
