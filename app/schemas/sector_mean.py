from datetime import date
from typing import Optional
from pydantic import BaseModel, validator, conint


class SectorMean(BaseModel):
    sector_name: Optional[str] = None
    year: Optional[conint(gt=1900, lt=date.today().year+1)] = None
    mean: Optional[float] = None

    class Config:
        orm_mode = True
        

class SectorMeanCreate(SectorMean):
    sector_name: str
    year: conint(gt=1900, lt=date.today().year+1)
    volume: float


class SectorMeanRead(SectorMean):
    id: int