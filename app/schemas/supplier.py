from datetime import date
from typing import Optional
from pydantic import BaseModel, confloat, conint, constr, validator


class Supplier(BaseModel):
    name: Optional[constr(min_length=1, max_length=120)] = None
    year: Optional[conint(gt=1900, lt=date.today().year+1)] = None
    guarantee_of_origin: Optional[bool] = False
    emission_factor: Optional[confloat(ge=0)] = None

    class Config:
        orm_mode = True


class SupplierCreate(Supplier):
    name: constr(min_length=1, max_length=120)
    year: conint(gt=1900, lt=date.today().year+1)
    guarantee_of_origin: bool = False
    emission_factor: confloat(ge=0)


class SupplierRead(Supplier):
    id: int