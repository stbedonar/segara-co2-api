from typing import Optional
from pydantic import BaseModel, constr, validator


class VehicleType(BaseModel):
    name: Optional[constr(min_length=3, max_length=4)] = None

    class Config:
        orm_mode = True
        

class VehicleTypeCreate(VehicleType):
    name: constr(min_length=3, max_length=4)


class VehicleTypeRead(VehicleType):
    id: int