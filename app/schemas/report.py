from datetime import date
from typing import List, Optional
from pydantic import BaseModel, conint, validator


class Report(BaseModel):
    simile_name: str
    year: conint(gt=1900, lt=date.today().year+1)
    organisation_name: str