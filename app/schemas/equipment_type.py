from typing import Optional
from pydantic import BaseModel, constr, validator


class EquipmentType(BaseModel):
    name: Optional[constr(min_length=1, max_length=120)] = None

    class Config:
        orm_mode = True
        

class EquipmentTypeCreate(EquipmentType):
    name: constr(min_length=1, max_length=120)


class EquipmentTypeRead(EquipmentType):
    id: int