from datetime import date
from typing import List, Optional
from pydantic import BaseModel, constr, validator, conint
from . import facility, user, fleet


class Organisation(BaseModel):
    name: Optional[constr(min_length=1, max_length=120)] = None
    logo: Optional[str] = None
    sector_name: Optional[str] = None
    base_year: Optional[conint(gt=1900, lt=date.today().year+1)] = None

    class Config:
        orm_mode = True
        

class OrganisationCreate(Organisation):
    name: constr(min_length=1, max_length=120)
    logo: str
    sector_name: str
    base_year: conint(gt=1900, lt=date.today().year+1)


class OrganisationRead(Organisation):
    id: int
    name: constr(min_length=1, max_length=120)
    logo: str
    sector_name: str
    base_year: conint(gt=1900, lt=date.today().year+1)
    facilities: List[facility.FacilityRead] = []
    fleets: List[fleet.FleetRead] = []
    users: List[user.User] = []