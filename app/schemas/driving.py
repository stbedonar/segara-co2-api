from typing import Optional
from pydantic import BaseModel, constr, validator


class Driving(BaseModel):
    name: Optional[constr(min_length=1, max_length=20)] = None

    class Config:
        orm_mode = True
        

class DrivingCreate(Driving):
    name: constr(min_length=1, max_length=20)


class DrivingRead(Driving):
    id: int