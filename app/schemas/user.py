from typing import List, Optional
from pydantic import BaseModel, constr, validator


class User(BaseModel):
    id: Optional[int] = None
    name: Optional[constr(min_length=1, max_length=120)] = None
    organisation_name: Optional[str] = None
    last_name: constr(min_length=1, max_length=60)
    email: constr(min_length=6, max_length=60)
    is_active: bool = True
    role_id: int

    class Config:
        orm_mode = True