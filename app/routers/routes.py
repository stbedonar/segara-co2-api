from datetime import date
from typing import List, Any

from fastapi import (
    Depends, HTTPException, APIRouter,
    File, UploadFile)
from sqlalchemy.orm import Session

from .. import models, dependencies, importer

from ..crud import route

from app.schemas.route import Route, RouteRead

import json


router = APIRouter()


@router.post("/routes/create/", response_model=RouteRead)
def create_route(new_route: Route, db: Session = Depends(dependencies.get_db)) -> Any:
    return route.create_route(db=db, route=new_route)


@router.post("/routes/import/all", response_model=List[RouteRead])
def import_routes(file: UploadFile = File(...), db: Session = Depends(dependencies.get_db),
    ) -> Any:
    routes = importer.import_csv(
        file=file,
        schema=Route,
        validate_fn=None,
        validate_args=None,
        save_fn=getattr(route, 'create_route'),
        save_args="route=record",
        db=db)
    return routes


@router.get("/routes/search/all/", response_model=List[RouteRead])
def read_routes(
    skip: int = 0, limit: int = 100, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    routes = route.get_routes(db, skip=skip, limit=limit)
    return routes


@router.get("/routes/search/id/", response_model=RouteRead)
def read_route(route_id: int, db: Session = Depends(dependencies.get_db)) -> Any:
    db_route = route.get_route_by_id(db, id=route_id)
    if db_route is None:
        raise HTTPException(status_code=404, detail="Route not found")
    return db_route


@router.get("/routes/search/vehicle/", response_model=List[RouteRead])
def read_route_by_vehicle(vehicle_id: int, db: Session = Depends(dependencies.get_db)) -> Any:
    db_routes = route.get_routes_by_vehicle_id(db=db, id=vehicle_id)
    if db_routes is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_routes


@router.get("/routes/search/dates/", response_model=List[RouteRead])
def read_route_between_dates(
    date1: date, date2: date, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    db_routes = route.get_routes_between_dates(db=db, date1=date1, date2=date2)
    if db_routes is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_routes


@router.get("/routes/search/driving/", response_model=List[RouteRead])
def read_route_by_driving(driving_id: int, db: Session = Depends(dependencies.get_db)) -> Any:
    db_routes = route.get_routes_by_driving_id(db=db, id=driving_id)
    if db_routes is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_routes


@router.put("/routes/edit/id/", response_model=RouteRead)
def update_route(
    route_id: int,
    updated_route: Route,
    db: Session = Depends(dependencies.get_db)
    ) -> Any:
    return route.update_route(db=db, id=route_id, route=updated_route)


@router.delete("/routes/delete/id/", response_model=RouteRead)
def delete_route(route_id: int, db: Session = Depends(dependencies.get_db)) -> Any:
    return route.delete_route(db=db, id=route_id)
