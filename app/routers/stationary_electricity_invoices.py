from datetime import date
from typing import List, Any

from fastapi import (
    Depends, HTTPException, APIRouter,
    File, UploadFile)
from sqlalchemy.orm import Session

from .. import models, dependencies, importer

from ..crud import stationary_electricity_invoice as invoice

from app.schemas.stationary_electricity_invoice import (
    StationaryElectricityInvoice,
    StationaryElectricityInvoiceCreate,
    StationaryElectricityInvoiceRead,
    StationaryElectricityInvoiceDelete
)

import json


router = APIRouter()


@router.post(
    "/stationary_electricity_invoices/create/", response_model=StationaryElectricityInvoiceRead
    )
def create_invoice(
    new_invoice: StationaryElectricityInvoiceCreate, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    return invoice.create_invoice(db=db, invoice=new_invoice)


@router.post(
    "/stationary_electricity_invoices/import/all/",
    response_model=List[StationaryElectricityInvoiceRead]
    )
def import_stationary_electricity_invoices(
    file: UploadFile = File(...),
    db: Session = Depends(dependencies.get_db),
    ) -> Any:
    stationary_electricity_invoices = importer.import_csv(
        file=file,
        schema=StationaryElectricityInvoice,
        validate_fn=None,
        validate_args=None,
        save_fn=getattr(invoice, 'create_invoice'),
        save_args="invoice=record",
        db=db)
    return stationary_electricity_invoices


@router.get(
    "/stationary_electricity_invoices/search/all/",
    response_model=List[StationaryElectricityInvoiceRead]
    )
def read_invoices(
    skip: int = 0, limit: int = 100, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    invoices = invoice.get_invoices(db, skip=skip, limit=limit)
    return invoices


@router.get(
    "/stationary_electricity_invoices/search/id/",
    response_model=StationaryElectricityInvoiceRead
    )
def read_invoice(id: str, db: Session = Depends(dependencies.get_db)) -> Any:
    db_invoice = invoice.get_invoice_by_id(db=db, id=id)
    if db_invoice is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_invoice


@router.get(
    "/stationary_electricity_invoices/search/number/",
    response_model=StationaryElectricityInvoiceRead
    )
def read_invoice_by_invoice_number(number: str, db: Session = Depends(dependencies.get_db)) -> Any:
    db_invoice = invoice.get_invoice_by_invoice_number(db=db, number=number)
    if db_invoice is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_invoice


@router.get(
    "/stationary_electricity_invoices/search/facility/",
    response_model=List[StationaryElectricityInvoiceRead]
    )
def read_invoice_by_facility(facility_id: int, db: Session = Depends(dependencies.get_db)) -> Any:
    db_invoices = invoice.get_invoices_by_facility_id(db=db, id=facility_id)
    if db_invoices is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_invoices


@router.get(
    "/stationary_electricity_invoices/search/dates/",
    response_model=List[StationaryElectricityInvoiceRead]
    )
def read_invoice_between_dates(
    date1: date, date2: date, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    db_invoices = invoice.get_invoices_between_dates(db=db, date1=date1, date2=date2)
    if db_invoices is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_invoices


@router.get(
    "/stationary_electricity_invoices/search/supplier/",
    response_model=List[StationaryElectricityInvoiceRead]
    )
def read_invoice_by_supplier(supplier_id: int, db: Session = Depends(dependencies.get_db)) -> Any:
    db_invoices = invoice.get_invoices_by_supplier_id(db=db, id=supplier_id)
    if db_invoices is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_invoices


@router.get(
    "/stationary_electricity_invoices/search/partial_co2/",
    response_model=List[StationaryElectricityInvoiceRead]
    )
def read_invoice_by_partial(
    partial: float, operator: str, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    if operator not in ["et", "lt", "gt"]:
        raise HTTPException(status_code=400, detail="Accepted operators: 'gt', 'lt' and 'et'")
    db_invoices = invoice.get_invoices_by_partial(db=db, partial=partial, operator=operator)
    if db_invoices is None:
        raise HTTPException(status_code=404, detail="Invoices not found")
    return db_invoices


@router.put(
    "/stationary_electricity_invoices/edit/id/", response_model=StationaryElectricityInvoiceRead
    )
def update_invoice(
    invoice_id: int,
    updated_invoice: StationaryElectricityInvoice,
    db: Session = Depends(dependencies.get_db)
    ) -> Any:
    return invoice.update_invoice(db=db, id=invoice_id, invoice=updated_invoice)


@router.delete(
    "/stationary_electricity_invoices/delete/id/", response_model=StationaryElectricityInvoiceDelete
    )
def delete_invoice(invoice_id: int, db: Session = Depends(dependencies.get_db)) -> Any:
    return invoice.delete_invoice(db=db, id=invoice_id)