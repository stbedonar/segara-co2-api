from typing import List, Any

from fastapi import Depends, HTTPException, APIRouter
from sqlalchemy.orm import Session

from .. import models, dependencies, importer

from ..crud import role

from app.schemas.role import Role


router = APIRouter()