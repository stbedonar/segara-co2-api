import os
import pdfkit

from fastapi import APIRouter, Request, Depends
from fastapi.responses import HTMLResponse, FileResponse
from fastapi.templating import Jinja2Templates

from sqlalchemy.orm import Session

from .. import models, dependencies

from ..report import StandardReport

from app.schemas.report import Report


router = APIRouter()

templates = Jinja2Templates(directory="app/templates")
template = templates.get_template("report.jinja2")


@router.post("/report/", response_class=FileResponse)
def create_report(
    request: Request, new_report: Report, db: Session = Depends(dependencies.get_db)
    ):
    report = StandardReport(
        db,
        simile_name=new_report.simile_name,
        year=new_report.year,
        organisation_name=new_report.organisation_name
        )

    tmp = "app/static/tmp/"

    html = template.render(
            request = request,
            organisation = report.organisation_name,
            comparison = report.comparison(),
            simile = report.simile_name,
            year = report.year,
            total = report.formatted_total,
            results = report.results,
            distribution = report.distribution(),
            # yearly_evolution: report.yearly_evolution(),
            base_year_comparison = report.base_year_comparison(),
            base_year = report.base_year,
            base_year_revenue_comparison = report.base_year_revenue_comparison(),
            sector_table = report.sector_table(),
            sector_chart = report.sector_chart(),
        )
    file = open(f"{tmp}report.html", 'w')
    file.write(html)
    file.close()

    pdfkit.from_file(f"{tmp}report.html", "file.pdf")

    for f in os.listdir(tmp):
        os.remove(os.path.join(tmp, f))

    return FileResponse(
                "file.pdf", media_type="application/pdf", filename="Report"
                )