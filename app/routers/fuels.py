from typing import List, Any

from fastapi import (
    Depends, HTTPException, APIRouter,
    File, UploadFile)
from sqlalchemy.orm import Session

from .. import models, dependencies, importer

from ..crud import fuel

from app.schemas.fuel import Fuel, FuelRead

import json


router = APIRouter()


@router.post("/fuels/create/", response_model=FuelRead)
def create_fuel(new_fuel: Fuel, db: Session = Depends(dependencies.get_db)) -> Any:
    db_fuel = fuel.get_fuel_by_name_and_year(db, name=new_fuel.name, year=new_fuel.year)
    if db_fuel:
        raise HTTPException(status_code=400, detail="Fuel already registered")
    return fuel.create_fuel(db=db, fuel=new_fuel)


@router.post("/fuels/import/all", response_model=List[FuelRead])
def import_fuels(file: UploadFile = File(...), db: Session = Depends(dependencies.get_db),
    ) -> Any:
    fuels = importer.import_csv(
        file=file,
        schema=Fuel,
        validate_fn=getattr(fuel, 'get_fuel_by_name_and_year'),
        validate_args="name=new_object.name,year=new_object.year",
        save_fn=getattr(fuel, 'create_fuel'),
        save_args="fuel=record",
        db=db)
    return fuels


@router.get("/fuels/search/all/", response_model=List[FuelRead])
def read_fuels(skip: int = 0, limit: int = 100, db: Session = Depends(dependencies.get_db)) -> Any:
    fuels = fuel.get_fuels(db, skip=skip, limit=limit)
    return fuels


@router.get("/fuels/search/id/", response_model=FuelRead)
def read_fuel(fuel_id: int, db: Session = Depends(dependencies.get_db)) -> Any:
    db_fuel = fuel.get_fuel_by_id(db, id=fuel_id)
    if db_fuel is None:
        raise HTTPException(status_code=404, detail="Fuel not found")
    return db_fuel


@router.get("/fuels/search/name/", response_model=List[FuelRead])
def read_fuel_by_name_and_year(
    name: str, year: int, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    db_fuel = fuel.get_fuel_by_name_and_year(db=db, name=name, year=year)
    if db_fuel is None:
        raise HTTPException(status_code=404, detail="Fuel not found")
    return db_fuel


@router.get("/fuels/search/year/", response_model=List[FuelRead])
def read_fuels_by_year(
    year: str, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    db_fuels = fuel.get_fuels_by_year(db=db, year=year)
    if db_fuels is None:
        raise HTTPException(status_code=404, detail="Fuels not found")
    return db_fuels


@router.get("/fuels/search/emission_factor/", response_model=List[FuelRead])
def read_fuel_by_emission_factor(
    emission_factor: str, operator: str, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    if operator not in ["et", "lt", "gt"]:
        raise HTTPException(status_code=400, detail="Accepted operators: 'gt', 'lt' and 'et'")
    db_fuels = fuel.get_fuels_by_emission_factor(
        db=db, emission_factor=emission_factor, operator=operator
        )
    if db_fuels is None:
        raise HTTPException(status_code=404, detail="Fuel not found")
    return db_fuels


@router.put("/fuels/update/id/", response_model=FuelRead)
def update_fuel(
    fuel_id: int, updated_fuel: Fuel, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    return fuel.update_fuel(db=db, id=fuel_id, fuel=updated_fuel)


@router.delete("/fuels/delete/id", response_model=FuelRead)
def delete_fuel(
    fuel_id: int, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    return fuel.delete_fuel(db=db, id=fuel_id)