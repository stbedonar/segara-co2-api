from typing import List, Any

from fastapi import (
    Depends, HTTPException, APIRouter,
    File, UploadFile)
from sqlalchemy.orm import Session

from .. import models, dependencies, importer

from ..crud import driving

from app.schemas.driving import DrivingRead, Driving

import json


router = APIRouter()


@router.post("/drivings/create/", response_model=DrivingRead)
def create_driving(
    new_driving: Driving, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    db_driving = driving.get_driving_by_name(db, name=new_driving.name)
    if db_driving:
        raise HTTPException(status_code=400, detail="Driving already registered")
    return driving.create_driving(db=db, driving=new_driving)


@router.post("/drivings/import/all", response_model=List[DrivingRead])
def import_drivings(file: UploadFile = File(...), db: Session = Depends(dependencies.get_db),
    ) -> Any:
    drivings = importer.import_csv(
        file=file,
        schema=Driving,
        validate_fn=getattr(driving, 'get_driving_by_name'),
        validate_args="name=new_object.name",
        save_fn=getattr(driving, 'create_driving'),
        save_args="driving=record",
        db=db)
    return drivings


@router.get("/drivings/search/all/", response_model=List[DrivingRead])
def read_drivings(
    skip: int = 0, limit: int = 100, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    drivings = driving.get_drivings(db, skip=skip, limit=limit)
    return drivings


@router.get("/drivings/search/id/", response_model=DrivingRead)
def read_driving_by_id(
    driving_id: int, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    db_driving = driving.get_driving_by_id(db, id=driving_id)
    if db_driving is None:
        raise HTTPException(status_code=404, detail="Driving not found")
    return db_driving


@router.get("/drivings/search/name/", response_model=DrivingRead)
def read_driving_by_name(name: str, db: Session = Depends(dependencies.get_db)) -> Any:
    db_driving = driving.get_driving_by_name(db, name=name)
    if db_driving is None:
        raise HTTPException(status_code=404, detail="Driving not found")
    return db_driving


@router.put("/drivings/update/id/", response_model=DrivingRead)
def update_driving(
    driving_id: int, updated_driving: Driving, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    return driving.update_driving(db=db, id=driving_id, driving=updated_driving)


@router.delete("/drivings/delete/id/", response_model=DrivingRead)
def delete_driving(driving_id: int, db: Session = Depends(dependencies.get_db)) -> Any:
    return driving.delete_driving(db=db, id=driving_id)