from typing import List, Any

from fastapi import (
    Depends, HTTPException, APIRouter,
    File, UploadFile)
from sqlalchemy.orm import Session

from .. import models, dependencies, importer

from ..crud import simile

from app.schemas.simile import Simile, SimileRead

import json


router = APIRouter()


@router.post("/similes/create/", response_model=SimileRead)
def create_simile(
    new_simile: Simile, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    db_simile = simile.get_simile_by_name(db, name=new_simile.name)
    if db_simile:
        raise HTTPException(status_code=400, detail="Simile already registered")
    return simile.create_simile(db=db, simile=new_simile)


@router.post("/similes/import/all", response_model=List[SimileRead])
def import_similes(file: UploadFile = File(...), db: Session = Depends(dependencies.get_db),
    ) -> Any:
    similes = importer.import_csv(
        file=file,
        schema=Simile,
        validate_fn=getattr(simile, 'get_simile_by_name'),
        validate_args="name=new_object.name",
        save_fn=getattr(simile, 'create_simile'),
        save_args="simile=record",
        db=db)
    return similes


@router.get("/similes/search/all/", response_model=List[SimileRead])
def read_similes(
    skip: int = 0, limit: int = 100, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    similes = simile.get_similes(db, skip=skip, limit=limit)
    return similes


@router.get("/similes/search/id/", response_model=SimileRead)
def read_simile(simile_id: int, db: Session = Depends(dependencies.get_db)) -> Any:
    db_simile = simile.get_simile_by_id(db, id=simile_id)
    if db_simile is None:
        raise HTTPException(status_code=404, detail="Simile not found")
    return db_simile


@router.get("/similes/search/name/", response_model=SimileRead)
def read_simile_by_name(name: str, db: Session = Depends(dependencies.get_db)) -> Any:
    db_simile = simile.get_simile_by_name(db, name=name)
    if db_simile is None:
        raise HTTPException(status_code=404, detail="Simile not found")
    return db_simile


@router.get(
    "/similes/search/volume/",
    response_model=List[SimileRead]
    )
def read_simile_by_volume(
    volume: float, operator: str, db: Session = Depends(dependencies.get_db)
    ) -> Any:
    if operator not in ["et", "lt", "gt"]:
        raise HTTPException(status_code=400, detail="Accepted operators: 'gt', 'lt' and 'et'")
    db_similes = simile.get_similes_by_volume(db=db, volume=volume, operator=operator)
    if db_similes is None:
        raise HTTPException(status_code=404, detail="Similes not found")
    return db_similes


@router.put("/similes/update/id/", response_model=SimileRead)
def update_simile(
    simile_id: int,
    updated_simile: Simile,
    db: Session = Depends(dependencies.get_db)
    ) -> Any:
    return simile.update_simile(db=db, id=simile_id, simile=updated_simile)


@router.delete("/similes/delete/id/", response_model=SimileRead)
def delete_simile(simile_id: int, db: Session = Depends(dependencies.get_db)) -> Any:
    return simile.delete_simile(db=db, id=simile_id)