import pandas as pd
import json
import csv
import shutil
import os
import ast

from fastapi import HTTPException

from pydantic import ValidationError


def import_csv(file, schema, validate_fn, validate_args, save_fn, save_args, db):
    # If the importer is called from a route
    if type(file) != str:
        data = parse_to_json(file)
    
    # If the importer is called from the app
    else:
       df = pd.read_csv(file, sep=";")
       result = df.to_json(orient="records")
       data = json.loads(result)

    records = validate_data(data, schema, validate_fn, db, validate_args)
    return save_data(records, save_fn, db, save_args)


def parse_to_json(file):
    # Checks whether the file is a CSV or not
    if file.content_type != "text/csv":
        raise HTTPException(status_code=400, detail="The uploaded file is not a CSV file")

    # Writes UploadFile object as temporary file so it can be sniffed 
    path = f"app/tmp/{file.filename}"
    with open(path, "wb") as buffer:
        shutil.copyfileobj(file.file, buffer)

    # Checks whether the delimiter is a semicolon or not
    # with open(path) as csvfile:
    #     try:
    #         dialect = csv.Sniffer().sniff(csvfile.read(1024), delimiters=";")
    #     except:
    #         raise HTTPException(
    #             status_code=400,
    #             detail="The separator must be a semicolon (';').")

    df = pd.read_csv(path, sep=";")
    os.remove(path)

    # Checks whether the dataframe has actual data or not
    if df.empty:
        raise HTTPException(status_code=400, detail="The uploaded file is empty")
    
    result = df.to_json(orient="records")
    parsed = json.loads(result)
    return parsed


def validate_data(data, schema, validate_fn, db, validate_args):
    records = []
    for object in data:

        # Lists are converted to strings, so we perform a literal
        # evaluation if the object has a string representation of a list
        for key in object:  
            if str(object[key]).startswith("["):
                object[key] = ast.literal_eval(object[key])

        # Validates the object against the provided Pydantic model
        try:
            new_object = schema.parse_obj(object)
        except ValidationError as exc:
            raise HTTPException(status_code=422, detail=repr(exc))

        # Checks whether the object already exists in the database or not
        if validate_fn and validate_args: # Not all the models need this validation
            kwargs = eval(f"dict({validate_args.replace(' ',',')})")
            db_object = validate_fn(db, **kwargs)
            if db_object:
                raise HTTPException(
                    status_code=400,
                    detail=f"Object already registered with id {db_object.id}")
    
        records.append(new_object)

    return records


def save_data(records, save_fn, db, save_args):
    saved = []
    for record in records:
        kwargs = eval(f"dict({save_args.replace(' ',',')})")
        saved.append(save_fn(db, **kwargs))
    return saved